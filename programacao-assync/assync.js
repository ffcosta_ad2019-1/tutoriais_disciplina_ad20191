function main() {
    funcaoUm();
    funcaoDois();
}

function funcaoUm() {
    setTimeout(() => { 
        console.log("Um"); 
    }, 2000);
}

function funcaoDois() {
    setTimeout(() => { 
        console.log("Dois"); 
    }, 1000);
}

main();