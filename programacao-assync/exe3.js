async function main() {
    let nome = await getNome(2000);
    let sobrenome = await getSobrenome(1000);

    console.log(nome + ' ' + sobrenome);
}

async function getNome(ms) {
    let result = await sleep("Fernando", ms);
    return result;
}

async function getSobrenome(ms) {
    let result = await sleep("Ferreira da Costa", ms);
    return result;
}

function sleep(value, ms = 0) {
  return new Promise(r => setTimeout(() => {r(value)}, ms));
}

main();