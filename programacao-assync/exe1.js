function main() {
    getName().then((data) => {
        console.log(data);
    });
}

function getName() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Fernando");
        }, 3000);
    });
}



main();