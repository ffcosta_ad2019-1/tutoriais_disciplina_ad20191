function main() {
    getSobrenome().then((data) => {
        console.log(data);
    });
}

function getSobrenome() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Ferreira da Costa");
        }, 2000);
    });
}

main();