function main() {
    funcaoUm().then((data) => {
        console.log(data);
        funcaoDois();
    });
}

function funcaoUm() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve("Um");
        }, 2000);
    });
}

function funcaoDois() {
    setTimeout(() => {
        console.log("Dois");
    }, 1000);
}

main();