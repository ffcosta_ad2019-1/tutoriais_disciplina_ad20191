# Tutoriais do Grupo de tutoriais Aplicações Distribuídas 2019/1

## Tutoriais realizados
- [Programação Assíncrona](#programação-assíncrona)

### Programação Assíncrona

Código no diretório programacao-assync

Passo a passo

1. Assíncronismo em Node.js
![assync](programacao-assync/img/assync.png)

2. Promisses
![promisse](programacao-assync/img/promisse.png)

3.  Async / Await
![assync_wait](programacao-assync/img/assync_wait.png)

4. Exercício 1)
![exe1](programacao-assync/img/exe1.png)

5. Exercício 2)
![exe2](programacao-assync/img/exe2.png)

6. Exercício 3)
![exe3](programacao-assync/img/exe3.png)

7. Issue criada
[Sugestão para o tutorial](https://gitlab.com/tetrinetjs/tutorials/issues/9)